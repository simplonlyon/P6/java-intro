package co.simplon.first;

import java.util.ArrayList;
import java.util.List;

public class Exemple {

	private String maProp;
	/**
	 * En Java, on doit indiquer la taille des array primitifs 
	 * au moment de leur initialisation, ils ne pourront pas faire
	 * ni plus ni moins que la taille qu'on leur indique (genre là
	 * si je veux mettre un dixième élément, c'est impossible)
	 */
	private int[] tab = new int[9];
	/**
	 * Si on veut des tableau qui ressemblent d'avantage à ce dont
	 * on a l'habitude en JS et en PHP, on utilisera plutôt les 
	 * objets List dont l'implémentation la plus utilisée est ArrayList
	 */
	private List<String> liste = new ArrayList<String>();
	
	public Exemple(String maProp) {
		super();
		this.maProp = maProp;
	}

	public Exemple() {
		super();
		this.maProp = "bloup";
	}
	
	public String presentation(String param) {
		
		return param + " " + this.maProp;
	}
	
	public void useTab() {
		tab[0] = 10;
		System.out.println(tab[0]);	
	}
	
	public void addItem(String item) {
		liste.add(item);
	}
	
	public void loop() {
		//Le for..of en java ressemble à ça, première partie la variable
		//qui sera accessible dans la boucle et son type, puis : et derrière
		//la liste/le tableau sur lequel on veut boucler
		for(String item: liste) {
			System.out.println(item);
		}
		//marche aussi sur les array primitifs
		for(int item: tab) {
			System.out.println(item);
		}
	}
}
